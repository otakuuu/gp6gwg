package com.gw.dao;

import com.gw.pojo.User;

public interface UserDao {

    public User selectById(Integer id);
}
