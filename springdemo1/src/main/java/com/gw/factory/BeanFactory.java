package com.gw.factory;

import com.gw.dao.UserDao;
import com.gw.pojo.User;

import javax.annotation.Resources;
import java.io.IOException;
import java.util.Properties;

public class BeanFactory {
    private Properties properties = new Properties();

    public BeanFactory() { }

    public BeanFactory(String path) throws IOException {
        properties.load(BeanFactory.class.getResourceAsStream(path));
    }

    public Object getBean(String beanName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String bean = properties.getProperty(beanName);
        Object ins = null;
        if (bean != null) {
            ins = Class.forName(beanName).newInstance();
        }
        return ins;
    }

    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        BeanFactory factory = new BeanFactory("/bean.properties");

        UserDao userDao = (UserDao) factory.getBean("userDao");

        User user = userDao.selectById(1);

        System.out.println(user);
    }
}
