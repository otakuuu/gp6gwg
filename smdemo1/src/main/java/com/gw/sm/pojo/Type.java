package com.gw.sm.pojo;

import java.io.Serializable;

/**
 * 对应的数据库的类别表
 */
public class Type  implements Serializable {


    private int  id;         // '类别的主键id',
    private String name;     // '类别的名称',
    private Integer level;   // 类型的级别123
    private Integer parent;  // 父级id, 上级序号，没有0

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "Type{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", level=" + level +
                ", parent=" + parent +
                '}';
    }

}
