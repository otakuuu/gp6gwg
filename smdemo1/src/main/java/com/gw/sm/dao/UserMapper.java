package com.gw.sm.dao;

import com.gw.sm.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserMapper {

    List<User> selectAll();

    @Select("select * from `tb_user` where username = #{username}")
    List<User> selectByUsername(String username);

    @Insert("insert into `tb_user`(username, password, gender, status)" +
            "values(#{username}, #{password}, #{gender}, #{status})")
    int insert(User user);
}
