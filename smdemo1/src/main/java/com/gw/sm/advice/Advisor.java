package com.gw.sm.advice;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Date;

@Aspect
@Component
public class Advisor {
    @Pointcut("execution(* com.gw.sm.service.UserServiceImpl.*(..))")
    public void pc(){}

    @Before("pc()")
    public void logStart(JoinPoint a) {
        System.out.println("========" + a.getTarget().getClass().getName() + "==begin===========");
    }

    @AfterReturning(value = "pc()", returning = "ret")
    public void logEnd(JoinPoint a, Object ret) {
        System.out.println("========" + a.getTarget().getClass().getName() + "==end===========");
    }

    @Around(value = "pc()")
    public Object logTime(ProceedingJoinPoint p) throws Throwable {
        long before = new Date().getTime();
        Object proceed = p.proceed();
        long after = new Date().getTime();
        System.out.println("共计用时：" + (after - before));
        return proceed;
    }
}
