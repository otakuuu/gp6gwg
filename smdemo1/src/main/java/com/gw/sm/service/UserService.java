package com.gw.sm.service;

import com.gw.sm.pojo.User;

import java.util.List;

public interface UserService {
    List<User> selectAll();

    List<User> selectByUsername(String username);

    int insert(User user);
}
