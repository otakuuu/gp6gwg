import com.gw.sm.dao.UserMapper;
import com.gw.sm.pojo.User;
import com.gw.sm.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestUserMapper {

    @Autowired
    UserService userService;

    @Test
    public void test() {
        System.out.println("SpringJUnit4ClassRunner");

        List<User> users = userService.selectAll();

        System.out.println(users);
    }

}
