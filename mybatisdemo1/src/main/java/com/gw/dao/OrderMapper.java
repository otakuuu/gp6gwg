package com.gw.dao;

import com.gw.pojo.Order;

import java.util.List;

public interface OrderMapper {
    List<Order> selectAll();
}
