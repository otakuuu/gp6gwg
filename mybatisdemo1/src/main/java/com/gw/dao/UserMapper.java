package com.gw.dao;

import com.gw.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    List<User> selectAll();

    List<User> selectBySex(String sex);

    List<User> selectLikeName(String username);

    User selectByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    User selectUserByUser(User user);

    User selectUserByMap(Map<String, String> map);

    int insert(User user);

    int update(User user);

    int deleteById(Integer id);

    List<User> selectAllUserAndDesc();

    List<User> selectAllUserAndOrder();

    List<User> selectAllUsersAll();

    List<User> selectByName(String name);

    @Select("select * from `t_users` where id = #{id};")
    User selectById(Integer id);
}
