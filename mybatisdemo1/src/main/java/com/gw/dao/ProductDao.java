package com.gw.dao;

import com.gw.pojo.Product;
import org.apache.ibatis.annotations.Select;

public interface ProductDao {

    @Select("select * from `t_product` where id = #{id}")
    Product selectByID(Integer id);
}
