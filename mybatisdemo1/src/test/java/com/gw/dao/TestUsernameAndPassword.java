package com.gw.dao;

import com.gw.pojo.User;
import com.gw.utils.MyBatisUtils;
import org.junit.Test;

import java.util.List;

public class TestUsernameAndPassword {

    @Test
    public void test() {
        UserMapper mapper = MyBatisUtils.getMapper(UserMapper.class);
        User user = mapper.selectByUsernameAndPassword("张三", "123");
        System.out.println(user);
    }

    @Test
    public void testSlectByName() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);
        List<User> zs = userMapper.selectByName("张三");
        System.out.println(zs);
    }

    @Test
    public void testAnnotiation() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);
        User user = userMapper.selectById(1);
        System.out.println(user);
    }
}
