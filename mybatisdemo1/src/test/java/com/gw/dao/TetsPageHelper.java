package com.gw.dao;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gw.pojo.User;
import com.gw.utils.MyBatisUtils;
import org.junit.Test;

import java.util.List;

public class TetsPageHelper {

    @Test
    public void testPageHelper() {
        UserMapper mapper = MyBatisUtils.getMapper(UserMapper.class);
        PageHelper.startPage(1, 3);
        List<User> users = mapper.selectAll();
        for (User user : users) {
            System.out.println("========" + user);
        }
    }
}
