package com.gw.dao;

import com.gw.pojo.User;
import com.gw.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class TestSelectAll {
    @Test
    public void testSelectAllUserAndOrder() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);
//        List<User> users = userMapper.selectAllUserAndOrder();
//        List<User> users = userMapper.selectAllUsersAll();
        List<User> users = userMapper.selectByName("张三");
        MyBatisUtils.closeSession();
        System.out.println(users);
    }
}