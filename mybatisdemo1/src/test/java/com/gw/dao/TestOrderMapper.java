package com.gw.dao;

import com.gw.pojo.Order;
import com.gw.utils.MyBatisUtils;
import org.junit.Test;

import java.util.List;

public class TestOrderMapper {

    @Test
    public void Test() {
        OrderMapper mapper = MyBatisUtils.getMapper(OrderMapper.class);

        List<Order> orders = mapper.selectAll();

        System.out.println(orders);
    }
}
