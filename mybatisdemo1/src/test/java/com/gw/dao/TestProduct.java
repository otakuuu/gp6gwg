package com.gw.dao;

import com.gw.pojo.Product;
import com.gw.utils.MyBatisUtils;
import org.junit.Test;

public class TestProduct  {

    @Test
    public void testAnnotiation() {
        ProductDao mapper = MyBatisUtils.getMapper(ProductDao.class);
        Product product = mapper.   selectByID(1);
        System.out.println(product);
    }
}
